import si from 'systeminformation';
import WebSocket from 'ws';

const SERVER_URL = 'ws://'+process.env.WSURL+'/input/'+process.env.SRVTOKEN;
const UPDATE_INTERVAL = 2 * 1000;

console.log('Connecting websocket to ' + SERVER_URL);
let ws = connectWebsocket();

function attachHandlers(ws) {
    ws.on('error', function(){
        if(ws._socket == null)
            ws = connectWebsocket();
    });

    ws.on('open', function open() {
        console.log("Connected !");
        const interval = setInterval( async function () {
            if (ws.readyState == ws.OPEN)
                sendMetrics(ws).catch(console.log);
            else
            {
                ws = connectWebsocket();
                clearInterval(interval);
            }
        }, UPDATE_INTERVAL);
    });
}

function connectWebsocket(){
    let wss = new WebSocket(SERVER_URL);
    attachHandlers(wss);
    return wss;
}

async function sendMetrics(ws)
{
   let data = {
       cpu_info: await si.cpu(),
       cpu_speed: await si.cpuCurrentspeed(),
       cpu_temp: await si.cpuTemperature(),
       mem: await si.mem(),
       os_info: await si.osInfo(),
       fs: await si.fsSize()
   };
   ws.send(JSON.stringify(data));
}
