# Le Surveilleur

Plus d'info sur le projet sur la page ensiwiki: https://ensiwiki.ensimag.fr/index.php?title=CAW1_2018_Projet_de_Nicolas_Pamart_et_Lo%C3%AFc_Wisniewski

## Comment lancer l'application ?

Aller à la racine du projet.
Taper:

``` 
cd le-surveilleur-front
npm install
cd ../le-surveilleur-back
npm install
cd ..
docker-compose down
docker-compose up
 ```

 Une fois lancée, le front est accessible à l'adresse http://localhost:8080/
 
 Le back-end est lancé dans un serveur à part, http://localhost:3000/

En même temps que le projet se lance, 3 faux serveurs sont lancés pour pouvoir tester les fonctionnalités de monitoring. Dans l'application, lorsque vous ajoutez un serveur, vous pouvez rentrer les identifiants suivants:

URL: machine1
Login: root
Password: root

Il y a aussi la machine2 et la machine3, pour faire des tests avec plusieurs personnes en même temps.

Au démarrage de l'application, un utilisateur administrateur est créé par défault, ses identifiants sont admin:admin (pas très sécuriser, mais pour tester c'est bien pratique).

## Comment lancer les tests cucumber ?

Aller à la racine du projet (ne pas oublier les npm install, voir plus haut)
```
docker-compose -f docker-compose-test-back.yml down && docker-compose -f docker-compose-test-back.yml up
```

Et grâce à la magie, tout devrais être vert.

## Comment lancer les tests selenium ?

Nous ne pouvons pas faire tourner ces tests dans un docker car ils utilisent le navigateur.
Il vous faut donc installer, sur votre machine :
	- nodeJS
	- npm
	- Chrome et/ou Firefox
	- drivers Chrome et/ou Firefox

Nous avons utilisé l'outil WebDriver(JS) de Selenium. Pour contrôler le navigateur Chrome ou Firefox, il a besoin des drivers de ces derniers.
Sur Linux, à partir de la racine du projet web (il vous faut wget d'installé):
```
cd le-surveilleur-selenium
./install_linux_drivers.sh
npm install
npm install mocha -g
./run_linux.sh
```

Pour Windows, suivez la procédure suivante : https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Cross_browser_testing/Your_own_automation_environment
