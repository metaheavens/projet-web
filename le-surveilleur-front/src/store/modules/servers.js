import Rainbow from 'rainbowvis.js';
import tinycolor from 'tinycolor2';

export default {
    namespaced: true,
    state: {
        serverList: [],
        serverListAdmin: [],
        userListAdmin: [],
        chartsStates: {}
    },
    mutations: {
        //Websocket caught mutations
        getAll() {},
        getAllAdmin() {},
        add() {},
        remove() {},
        edit() {},

        //Client mutations
        updateServerList(state, servers) {
            state.serverList = servers;
        },
        updateServerListAdmin(state, payload) {
            state.serverListAdmin = payload.servers;
            state.userListAdmin = payload.users;
        },
        info(state, payload) {
            const server = payload.server;
            const serverData = payload.data;
            let previousState = {};
            if(state.chartsStates[server.id]) previousState = Object.assign({}, state.chartsStates[server.id]);

            //Updating CPU Chart
            const date = new Date();
            if(!previousState.cpuChart) previousState.cpuChart = {
                labels: new Array(30).fill(''),
                datasets: [
                    {
                        label: 'CPU',
                        backgroundColor: '#2b95f8',
                        data: new Array(30)
                    }
                ]
            };
            previousState.cpuChart.labels.shift();
            previousState.cpuChart.labels.push(date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds());
            previousState.cpuChart.datasets[0].data.shift();
            previousState.cpuChart.datasets[0].data.push(serverData.cpu_speed.avg);

            previousState.diskChart = {
                labels: [],
                datasets: [
                    {
                        label: 'Disques',
                        data: []
                    }
                ]
            };
            const data = previousState.diskChart;
            const rainbow = new Rainbow();

            serverData.fs.forEach(fs => {
                data.labels.push(fs.fs + " Free");
                data.labels.push(fs.fs + " Used");
                data.datasets[0].data.push(100 - fs.use);
                data.datasets[0].data.push(fs.use);

            });

            rainbow.setNumberRange(0, data.labels.length - 1);
            rainbow.setSpectrum("#f44336", "#E91E63", "#9C27B0", "#673AB7", "#3F51B5", "#2196F3", "#03A9F4", "#00BCD4", "#009688", "#4CAF50");
            let color = [];

            function getColorPrimary(index) {
                return '#' + rainbow.colourAt(index);
            }

            function getColorSecondary(index) {
                return tinycolor('#' + rainbow.colourAt(index)).lighten().toHexString();
            }

            for(let i = 0; i < data.labels.length; i = i + 2) {
                color.push(getColorPrimary(i));
                color.push(getColorSecondary(i));
            }

            data.datasets[0].backgroundColor = color;
            previousState.diskChart = data;

            state.chartsStates = {...state.chartsStates, [server.id]: previousState};
        }
    }

};
