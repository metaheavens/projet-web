import Vuex from 'vuex'
import servers from './modules/servers';
import Vue from 'vue';
import createWebSocketPlugin from './plugin/websocket'
import config from '../config/config'
import request from 'superagent'
import ReconnectingWebsocket from 'reconnecting-websocket';

Vue.use(Vuex);

/**
 * Returns vuex store if the user is authenticated.
 *
 * @returns {Promise<Store>}
 */
async function getStore() {
    const isAuth = await request.get('http://' + config.serverAdress + '/authorized')
        .withCredentials()
        .then(res => res.body)
        .catch(() => undefined);
    if (!isAuth) throw "Unauthorized";
    else {
        const ws = await getWebSocket("ws://" + config.serverAdress + "/");
        return new Vuex.Store({
            state: {
                isAdmin: isAuth.isAdmin
            },
            modules: {
                servers
            },
            plugins: [createWebSocketPlugin(ws)]
        });
    }
}

/**
 * Returns a (Promise) websocket that autoreconnect when server closes it.
 * It is useful for development and error handling.
 *
 * @returns {Promise<ReconnectingWebsocket>}
 */
function getWebSocket(url) {
    return new Promise(function (resolve, reject) {
        const ws = new ReconnectingWebsocket(url, [], {debug: true});
        ws.onopen = () => {
            if (ws.reconnectionInterval) {
                ws.avoidReconnection = false;
                clearInterval(ws.reconnectionInterval);
            }
            resolve(ws);
        };
        ws.onclose = () => {
            if (!ws.avoidReconnection) {
                ws.reconnectionInterval = setInterval(() => ws.reconnect(), 1000);
                ws.avoidReconnection = true;
            }
        };
        ws.onerror = reject;
    });

}

export default getStore;