export default function createWebSocketPlugin (socket) {
    return store => {
        store.subscribe(mutation => {
            if (mutation.type === 'servers/add') {
                socket.send(JSON.stringify(mutation))
            }
            else if (mutation.type === 'servers/getAll') {
                socket.send(JSON.stringify(mutation))
            }
            else if (mutation.type === 'servers/remove') {
                socket.send(JSON.stringify(mutation))
            }
            else if (mutation.type === 'servers/edit') {
                socket.send(JSON.stringify(mutation))
            }
            else if (mutation.type === 'servers/getAllAdmin') {
                socket.send(JSON.stringify(mutation))
            }
        });

        socket.onmessage = function(rep) {
            const message = JSON.parse(rep.data);
            console.log(message);
            if(message.type === 'serverList'){
                store.commit('servers/updateServerList', message.payload)
            }
            else if(message.type === 'serverListAdmin'){
                store.commit('servers/updateServerListAdmin', message.payload)
            }
            else if(message.type === 'serverInfo'){
                store.commit('servers/info', message.payload)
            }
        };
    }
}