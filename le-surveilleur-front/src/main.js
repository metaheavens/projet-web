import Vue from 'vue'
import MainComponent from './MainComponent.vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import 'vuetify/dist/vuetify.min.css'
import Dashboard from './components/app/Dashboard'
import ServerDashboard from './components/app/ServerDashboard'
import App from './components/app/App'
import AppLogin from './components/login/AppLogin'
import Login from './components/login/Login'
import Register from './components/login/Register'
import getStore from './store'
import AdminDashboard from './components/app/AdminDashboard'

Vue.config.productionTip = false;
Vue.use(Vuetify);
Vue.use(VueRouter);

getStore().then(startApp).catch(startLogin);

function startApp(store) {

    let router = new VueRouter({
        routes: [
            {
                path: '/app',
                name: 'Application',
                component: App,
                children: [
                    {
                        path: 'dashboard',
                        component: Dashboard,
                    },
                    {
                        path: 'server/:id',
                        component: ServerDashboard
                    },
                    {
                        path: 'admin-dashboard',
                        component: AdminDashboard
                    }
                ]
            },
            {path: '*', redirect: '/app/dashboard'}
        ]
    });

    new Vue({
        render: h => h(MainComponent),
        store,
        router
    }).$mount('#app');
}

function startLogin() {

    let router = new VueRouter({
        routes: [
            {
                path: '/login',
                name: 'Login',
                component: Login
            },
            {
                path: '/register',
                name: 'Register',
                component: Register
            },
            {path: '*', redirect: '/login'}
        ]
    });

    new Vue({
        render: h => h(AppLogin),
        router
    }).$mount('#app');
}