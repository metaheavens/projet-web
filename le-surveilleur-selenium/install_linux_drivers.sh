#!/usr/bin/env bash
CHROME_URL="http://chromedriver.storage.googleapis.com/2.9/chromedriver_linux64.zip"
FIREFOX_URL="https://github.com/mozilla/geckodriver/releases/download/v0.20.1/geckodriver-v0.20.1-linux64.tar.gz"

mkdir drivers && cd drivers
# Note: using wget because of github's redirection
wget $CHROME_URL -O chrome.zip
wget $FIREFOX_URL -O firefox.tar.gz

unzip chrome.zip
tar -xf firefox.tar.gz

EXPORT_CMD='export PATH="$PATH:$(pwd)/drivers"'
echo -e "*********************************************\n"
echo "Drivers installed in $(pwd) !"
echo "Now run don't forget to run '$EXPORT_CMD'"
echo "Add '$EXPORT_CMD' at the end of your .*rc file if you want it to be persistent"
