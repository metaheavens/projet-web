var assert = require('assert'),
    webdriver = require('selenium-webdriver');

/*
 *	Le-surveilleur Selenium tests using Mocha.
 */

const appUrl = "http://localhost:8080";
const loginAppUrl = "http://localhost:8080/#/login";

const userLogin = "foo";
const userPassword = "password";

const driver = new webdriver.Builder()
	.withCapabilities(webdriver.Capabilities.firefox())
	.build();

const until = webdriver.until;

describe('Le Surveilleur tests', function() {
	const displayTimeout = 20000;
	this.timeout(20000);

	before(function() {
		return driver.get(appUrl);
	});


	function waitForVisibility(element) {
		return new Promise(function(resolve, reject) {
			const x = driver.wait(until.elementLocated(element), displayTimeout).then(e => {
				driver.wait(
					until.elementIsVisible(e),
					displayTimeout
				).then(e => {
					resolve(e);
				});
			});
		});
	};


	it('register', function() {
		/* Register a User
		 */
		const registerButtonId = "register";
		const loginFieldNameR = "loginR";
		const passwordFieldNameR = "passwordR";
		const passwordCheckFieldName = "passwordCheckR";

		return new Promise(function(resolve, reject) {
			/* Click on "S'inscire" button
			 */
			waitForVisibility(webdriver.By.id(registerButtonId)).then(element => {
				element.click();
				/* Wait for register form to be visible
				*/
				waitForVisibility(webdriver.By.name(loginFieldNameR)).then(element => {
					/* Send login
					*/
					element.sendKeys(userLogin);
					waitForVisibility(webdriver.By.name(passwordFieldNameR)).then(element => {
						/* Send password
						*/
						element.sendKeys(userPassword);
						waitForVisibility(webdriver.By.name(passwordCheckFieldName)).then(element => {
							/* Send password confirmation
							*/
							element.sendKeys(userPassword);
							waitForVisibility(webdriver.By.id(registerButtonId)).then(element => {
								/* Click register button
								*/
								element.click();
								resolve();
							});
						});
					});
				});
			});
		});
	});

	it('connection', function() {
		/* Connect a User
		 */
		const loginButtonId = "connect";
		const loginFieldName = "login";
		const passwordFieldName = "password";

		return new Promise(function(resolve, reject) {
			/* Load new webpage
			*/
			driver.get(appUrl).then(() => {
				/* Wait for login form to be visible
				*/
				waitForVisibility(webdriver.By.name(loginFieldName)).then(element => {
					/* Send login
					*/
					element.sendKeys(userLogin);
					waitForVisibility(webdriver.By.name(passwordFieldName)).then(element => {
						/* Send password
						*/
						element.sendKeys(userPassword);
						waitForVisibility(webdriver.By.id(loginButtonId)).then(element => {
							/* Click register button
							*/
							element.click();
							driver.sleep(5000).then(() => {
								resolve();
							});
						});
					});
				});
			});
		});
	});

});
