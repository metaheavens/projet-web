const {defineSupportCode} = require('cucumber');
const superagent = require('superagent');
const WebSocket = require('ws');

request = superagent.agent();
let websocket = null;
let sessionCookie = '';

const url = process.env.BACKEND_URL ? process.env.BACKEND_URL : "localhost:3000";

const addServer = afterWebsocketConnected(() => {
    return new Promise((resolve, reject) => {
        websocket.on('message', () => resolve());
        websocket.send(JSON.stringify({
            "type": "servers/add",
            "payload": {"url": "toto", "name": "toto", "user": "toto", "password": "toto"}
        }), err => {if(err) reject(err)});
    })
});

const removeServer = afterWebsocketConnected(async () => {
    const servers = await getServers();
    return new Promise(function(resolve, reject) {
        if(servers.length === 0) reject(Error("There is no server to remove"));
        websocket.on('message', m => {
            const parsed = JSON.parse(m);
            if(parsed.payload.length < servers.length) resolve()
        });
        websocket.send(JSON.stringify({
            "type": "servers/remove",
            "payload": servers[0].id
        }), err => {if(err) reject(err)});
    });
});

const getServers = afterWebsocketConnected(() => {
    return new Promise(function (resolve) {
        websocket.on('message', message => {
            const parsed = JSON.parse(message);
            resolve(parsed.payload);
        });
        websocket.send(JSON.stringify({type: "servers/getAll"}));
    });
});

function register() {
    return request.post('http://' + url + '/register')
        .withCredentials()
        .send({username: 'test', password: 'test'})
}

function startWebsocket(req) {
    if (!websocket) {
        sessionCookie = req.request.cookies;
        websocket = new WebSocket(
            'ws://' + url + '/',
            [],
            {
                'headers': {
                    'Cookie': sessionCookie
                }
            }
        );
        websocket.on('close', () => console.error("ERROR CONNECTION"));
    }
}

async function authorized() {
    const req = await request.get('http://' + url + '/authorized').withCredentials();
    startWebsocket(req);
}

function login() {
    return request.post('http://' + url + '/login')
        .withCredentials()
        .send({username: 'test', password: 'test'})
        .catch(() => {throw Error("Impossible de se connecter, veuillez réessayer.")});
}

const isServerListSizeIs = afterWebsocketConnected((size) => {
    return new Promise(function (resolve, reject) {
        websocket.on('message', message => {
            const parsed = JSON.parse(message);
            if (parsed.payload.length === size) resolve();
            else reject(Error("There is not " + size + " servers"));
        });
        websocket.send(JSON.stringify({type: "servers/getAll"}));
    });
});

function afterWebsocketConnected(func) {
    return function () {
        const args = arguments;
        return new Promise(function (resolve, reject) {
            if (websocket.readyState === 1)
                func(...args).then(resolve).catch(reject);
            else
                websocket.on('open', () => setTimeout(() => func(...args).then(resolve).catch(reject), 100));
        });
    }
}

defineSupportCode(function ({And, But, Given, Then, When}) {
    Given(/^An empty list of servers$/, () => isServerListSizeIs(0));
    Given(/^A logged user$/, () => register().then(login).catch(login).then(authorized));
    When(/^I add a new server$/, () => addServer());
    Then(/^I end up with (\d+) server in the list$/, nbServer => isServerListSizeIs(nbServer));
    Given(/^A list with one server$/, () => {
        isServerListSizeIs(1).then(e => e).catch(addServer).then(isServerListSizeIs(1))
    });
    When(/^I remove the server$/, () => removeServer());
});