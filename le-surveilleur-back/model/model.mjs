import Sequelize from 'sequelize';
import bcrypt from "bcrypt";

/**
 * Here is our model definition, tables are generated at server's runtime
 */

// Setting up ORM Sequelize
export const sequelize = new Sequelize('postgres', 'postgres', 'example', {
    host: 'database',
    dialect: 'postgres',

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

export const Sessions = sequelize.define('session', {
    sid: {type: Sequelize.STRING, unique: true, allowNull: false},
    sess: {type: Sequelize.JSON, allowNull: false},
    expire: {type: Sequelize.DATE, allowNull: false},
}, {
    timestamps: false,
    freezeTableName: true
});

export const User = sequelize.define('user', {
    username: {type: Sequelize.STRING, unique: true, allowNull: false},
    hash: Sequelize.STRING,
    isAdmin: { type: Sequelize.BOOLEAN, allowNull: false}
});

//Creating default admin user (not really safe, just for demo purpose)
bcrypt.hash('admin', 10).then(hashed => {
    User.create({
        username: 'admin',
        hash: hashed,
        isAdmin: 1
    }).catch(() => undefined);
});

export const Server = sequelize.define('server', {
    name: Sequelize.STRING,
    url: Sequelize.STRING,
    password: Sequelize.STRING,
    username: Sequelize.STRING,
    token: { type: Sequelize.STRING, unique: true },
    port: Sequelize.INTEGER
});

User.Server = User.hasMany(Server);
Server.User = Server.belongsTo(User, {as: 'UserServer', foreign_key:'user_server_id', constraints: false});

