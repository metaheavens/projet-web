export class ErrorMessage {
    constructor(code, message) {
        this.type = 'error';
        this.payload = {code: code, message: message}
    }
}

export const ErrorTypes = {
    USER_EXISTS: 1,
    SERVER_UNREACHABLE: 2,
    NOT_ADMIN: 3
};