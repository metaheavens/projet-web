import {Server, User} from "../model/model.mjs";
import Client from "ssh2";
import util from 'util';
import {ErrorMessage, ErrorTypes} from "../messages/ErrorMessage.mjs";
import uuidv1 from 'uuid/v1';

/*
    TODO: Add conf token
 */
const DOMAIN_NAME = "localhost";
const APP_PORT = 3000;

const SSH_COMMAND = 'curl %s/install_script/%s | bash &';
const SERVER_URL_SSH = "$(echo $SSH_CLIENT | awk '{ print $1}'):3000";

/**
 * Handle all message from the websocket that concern servers.
 */
async function serversHandler(type, message, ws, user) {
    const typeParams = type.split('/');
    switch (typeParams[0]) {
        case "add": {
            const server = {...message, token: uuidv1()};
            Server.create(server).then(async function (srv, err) {
                if (err) {
                    console.log(err);
                } else {
                    await user.addServers(srv);
                    try {
                        dropDaPayload(srv, ws);
                    } catch (e) {
                        console.error(e);
                    } finally {
                        ws.send(JSON.stringify({type: "serverList", payload: await user.getServers()}));
                    }
                }
            });
            break;
        }
        case "getAll":
            console.log(user.getServers());
            ws.send(JSON.stringify({type: "serverList", payload: await user.getServers()}));
            break;
        case "getAllAdmin":
            if (user.isAdmin)
                ws.send(JSON.stringify({
                    type: "serverListAdmin",
                    payload: {
                        servers: await getAllServers(),
                        users: await getAllUsers()
                    }
                }));
            else
                ws.send(JSON.stringify(new ErrorMessage('Not log as admin', ErrorTypes.NOT_ADMIN)));
            break;
        case "remove": {
            let seqServer = await Server.find({where: {id: message}});
            if (seqServer)
                await user.removeServers(seqServer);
            ws.send(JSON.stringify({type: "serverList", payload: await user.getServers()}));
            break;
        }
        case "edit": {
            if (await Server.update(message, {
                    where: {
                        id: message.id
                    }
                }))
                ws.send(JSON.stringify({type: "serverList", payload: await user.getServers()}));
            break;
        }
        default:
            console.warn("Unhandled message because of wrong type" + typeParams[0] + " unknown");
    }
}

/**
 * Log with SSH in an added server, and CURL the backend to get the script and launch it.
 */
function dropDaPayload(server, ws) {
    let conn = new Client();
    conn.on('ready', function () {
        conn.exec(util.format(SSH_COMMAND, SERVER_URL_SSH, server.token), function (err, stream) {
            if (err) {
                console.log(err);
                ws.send(JSON.stringify({
                    type: "installInfo",
                    payload: {
                        text: "Could not connect to server via ssh",
                        type: "error"
                    }
                }));
            }
            stream.on('close', function (code, signal) {
                console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
                conn.end();
            }).on('data', function (data) {
                console.log('STDOUT: ' + data);
                ws.send(JSON.stringify({
                    type: "installInfo",
                    payload: {
                        text: data,
                        type: "info"
                    }
                }));
            }).stderr.on('data', function (data) {
                console.log('STDERR: ' + data);
                ws.send(JSON.stringify({
                    type: "installInfo",
                    payload: {
                        text: data,
                        type: "error"
                    }
                }));
            });
        });
    }).connect({
        host: server.url,
        port: server.port,
        username: server.username,
        password: server.password
    });
    conn.on('error', () => {
        try {
            ws.send(JSON.stringify(new ErrorMessage(ErrorTypes.SERVER_UNREACHABLE, "Failed to establish connection with given URL.")))
        } catch (err) {
            console.log(err)
        }
    })
}

function getAllServers() {
    return Server.findAll({
        raw: true
    });
}

function getAllUsers() {
    return User.findAll({
        raw: true
    });
}

export default serversHandler;