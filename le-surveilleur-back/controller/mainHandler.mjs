import serversHandler from "./serversHandler.mjs";

/**
 * This is our main handler. For now it have only one entry point, but if other entities than server
 * was managed, routing should be here.
 */
function mainHandler(type, message, ws, user) {
    const typeParams = type.split('/');
    let nextHandler = () => {
    };
    switch (typeParams[0]) {
        case "servers":
            nextHandler = serversHandler;
            break;
        default:
            console.warn("Unhandled message because of wrong type" + typeParams[0] + " unknown");
    }

    nextHandler(typeParams.slice(1).join('/'), message, ws, user)
}

export default mainHandler;