import express from 'express';
import expressWs from 'express-ws';
import {User, Server} from './model/model';
import {sequelize} from "./model/model.mjs";
import passport from 'passport';
import Strategy from 'passport-local';
import bodyParser from 'body-parser';
import session from 'express-session';
import mainHandler from './controller/mainHandler';
import {ErrorMessage, ErrorTypes} from "./messages/ErrorMessage";
import pgSession from 'connect-pg-simple';
import bcrypt from 'bcrypt';
import child_process from 'child_process';
import util from 'util';
import Rx from 'rxjs/Rx';

const exec = util.promisify(child_process.exec);

const PORT = 3000; // CONF FILE
const MONITOR_ARCHIVE_PATH = '/code/monitor.tgz'; // CONF FILE

const saltRound = 10; //PUT IT IN CONF FILE
const sessionSecret = "my-secret-put-it-in-a-config-file"; //PUT IT IN CONF FILE

const serversWS = new Map();
const usersWS = new Map();

// Waiting that sequelize starts then start the web server
sequelize.sync().then(startServer);

function startServer() {
    const app = express();
    const sessionKeeper = pgSession(session);
    app.use(session({
        store: new sessionKeeper({
            conString: 'postgresql://postgres:example@database:5432/postgres'
        }),
        secret: sessionSecret
    }));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    expressWs(app);

    app.use(function (req, res, next) {
        res.header('Access-Control-Allow-Origin', req.get('Origin') || '*');
        res.header('Access-Control-Allow-Credentials', 'true');
        res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
        res.header('Access-Control-Expose-Headers', 'Content-Length');
        res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
        if (req.method === 'OPTIONS') {
            return res.sendStatus(200);
        } else {
            return next();
        }
    });

    /**
     * Setting up passport authentication
     */
    passport.use(new Strategy(function (username, password, cb) {
        User.find({where: {username: username}}).then((user, err) => {
            if (err) {
                console.error("Error when fetching user.");
                console.error(err);
                return cb(err);
            }
            if (!user) {
                console.error("No user found when fetching user.");
                return cb(null, false);
            }

            bcrypt.compare(password, user.hash).then(isCorrect => {
                if (!isCorrect) {
                    console.log("Error typing password");
                    cb(null, false)
                }
                else return cb(null, user);
            });
        });
    }));

    /**
     * Setting up passport user serialization
     */
    passport.serializeUser(function (user, cb) {
        cb(null, user.id);
    });

    passport.deserializeUser(function (id, cb) {
        User.find({where: {id: id}}).then((user, err) => {
            if (err) {
                console.error("Fail to deserealize user.");
                console.error(err);
                return cb(err);
            }
            cb(null, user);
        });
    });

    app.use(passport.initialize());
    app.use(passport.session());

    app.post('/login',
        passport.authenticate('local'),
        function (req, res) {
            console.log(req.user.username + ' logged in');
            res.status(204).end();
        });

    app.get('/logout', (req, res) => {
        console.log(req.user.username + "logging out");
        req.logout();
        res.status(204).end();
    });

    app.get('/authorized', (req, res) => {
        if (!req.user) res.status(401).send({});
        else {
            console.debug("User " + req.user.username + " is authorized");
            res.status(200).send({isAdmin: req.user.isAdmin})
        }
    });

    /**
     * Creates user in database. It hashes the password to securely store it.
     */
    app.post('/register', async (req, res) => {
        if (!req.body.username || !req.body.password) res.status(400).end();
        const hashed = await bcrypt.hash(req.body.password, saltRound);
        const user = {username: req.body.username, hash: hashed};
        User.create({...user, isAdmin: false})
            .then(user => res.send(user))
            .catch(e => {
                if (e.name !== "SequelizeUniqueConstraintError") res.status(500);
                else res.status(400).send(new ErrorMessage(ErrorTypes.USER_EXISTS, "User already exists."))
            })

    });

    /**
     * This route is used to provide the monitor part. When a server is added, SSH connection is done
     * and a CURL to this route is done to get the monitoring script.
     */
    app.get('/monitor.tgz', async function (req, res) {
        /*
            TODO: remove this code in production (lol we will forget for sure bro)
         */
        console.log("- Compressing monitor code");
        let cmd = 'tar --exclude node_modules --exclude package-lock.json -czf monitor.tgz /monitor';
        const {stdout, stderr} = await exec(cmd);
        console.log('stdout:', stdout);
        console.log('stderr:', stderr);
        /*
            End TODO
         */
        console.log("- Sending archive");
        res.sendFile(MONITOR_ARCHIVE_PATH);
    });

    /**
     * This route is used, with the previous one, to install all what we need on a monitored server.
     * This one is used to give a script to execute on the server.
     */
    app.get('/install_script/:token', (req, res) => {
        console.log("TOKEN" + req.params.token);
        /*
            TODO: req.params contains the token of the server, add it in the following script in order to auth to server
         */
        const cmd = 'curl ' + getAppUrl(req) + '/monitor.tgz -o monitor.tgz\n'
            + 'tar -xf monitor.tgz\n'
            + 'cd monitor\n'
            + 'npm install\n'
            + 'export WSURL="$(echo $SSH_CLIENT | awk \'{ print $1}\'):3000"\n'
            + 'export SRVTOKEN=' + req.params.token + "\n"
            + 'screen -m -d npm start\n';
        res.status(200).send(cmd);
    });

    /**
     * This is our main entry point in the application. It opens a websocket with the front-end application
     * and all data (except for authentication) pass here.
     */
    app.ws('/', async function (ws, req) {
        if (!req.user) ws.close();
        else {
            console.log("WEBSOCKET CONNECTED " + req.user.username);
            usersWS.set(req.user.id, ws);

            //Getting servers according to our admin level
            let serveurs = [];
            if (!req.user.isAdmin) serveurs = req.user.getServers();
            else serveurs = Server.findAll();

            //Subscribing to all servers to get information on it.
            serveurs.map(server => {
                console.log("SUBSCRIBING");
                console.log(server);
                let s = serversWS.get(server.token);
                if (s) {
                    let sub = s.subscribe(function (msg) {
                        try {
                            ws.send(JSON.stringify(msg));
                        } catch (err) {
                            console.log(err);
                            sub.unsubscribe();
                        }
                    });
                    console.log(server.name + " subscribed from client connection");
                }
            });

            //All messages received from the websocket are sent to handleMessage
            ws.on('message', async function (msg) {
                console.log("Message: " + msg);
                await handleMessage(ws, msg, req.user);
            })
        }
    });

    /**
     * Entry point for monitor service. All monitored data, once installed, send data through here.
     */
    app.ws('/input/:token', async function (ws, req) {
        console.log('A server has connected with token : ' + req.params.token);
        let serverWS = new Rx.Observable.fromEvent(ws, "message");

        let server = await Server.findAll({
            raw: true,
            where: {
                token: req.params.token
            }
        });

        //Send data to all users that are allowed to see information of this server
        if (server.length > 0) {
            let enhancedData = serverWS.map(event => {
                return {type: "serverInfo", payload: {server: server[0], data: JSON.parse(event.data)}}
            });

            let userWS = usersWS.get(server[0].userId);
            if (userWS) {
                let sub = enhancedData.subscribe(function (msg) {
                    try {
                        userWS.send(JSON.stringify(msg));
                    } catch (err) {
                        console.log(err);
                        sub.unsubscribe();
                    }
                });
                serversWS.set(req.params.token, enhancedData);
                console.log("Subscribed from server");
            }
        }
    });


    async function handleMessage(ws, msg, user) {
        const parsedMessage = JSON.parse(msg);
        mainHandler(parsedMessage.type, parsedMessage.payload, ws, user);
    }

    app.listen(PORT, function () {
        console.log('Le Surveilleur listening on port ' + PORT);
    });

    function getAppUrl(req) {
        return req.protocol + '://' + req.get('host');
    }

}
